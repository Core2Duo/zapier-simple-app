from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils import timezone
from random import choice
import string


def generate_api_key():
    return ''.join([choice(string.digits + string.ascii_letters) for _ in range(32)])


class UserManager(BaseUserManager):
    def create_user(self, email, password=None):
        if not email:
            raise ValueError('You should enter user\' email')

        user = self.model(email=self.normalize_email(email))
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password):
        if not email:
            raise ValueError('You should enter user\' email')

        user = self.model(email=self.normalize_email(email), is_superuser=True, is_staff=True)
        user.set_password(password)
        user.save()
        return user


class User(AbstractBaseUser, PermissionsMixin):
    USERNAME_FIELD = 'email'

    objects = UserManager()

    email = models.EmailField('email', unique=True)

    registered = models.DateTimeField(default=timezone.now)
    is_staff = models.BooleanField('staff status', default=False,
                                   help_text='Designates whether the user can log into this admin site.')
    is_active = models.BooleanField('активен', default=True,
                                    help_text='Designates whether this user should be treated as '
                                              'active. Unselect this instead of deleting accounts.')

    api_key = models.CharField(max_length=32, default=generate_api_key)

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email


class MessageManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().order_by('-created')


class Message(models.Model):
    text = models.TextField()
    created = models.DateTimeField(default=timezone.now)

    objects = MessageManager()


class Subscription(models.Model):
    user = models.ForeignKey(User, db_index=True)
    event = models.CharField(max_length=64)
    url = models.CharField(max_length=128, unique=True)
