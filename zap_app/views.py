from django.views.generic import View, TemplateView, FormView, CreateView, ListView
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import login, authenticate

from .forms import UserCreationForm
from .models import Message


class IndexView(TemplateView):
    template_name = 'zap_app/index.html'

    def get_context_data(self, **kwargs):
        return {}


class UserProfileView(TemplateView):
    template_name = 'zap_app/user_profile.html'

    def get_context_data(self, **kwargs):
        return {}


class UserRegisterView(FormView):
    form_class = UserCreationForm
    template_name = 'zap_app/user_register.html'
    success_url = reverse_lazy('zap:user_profile')

    def form_valid(self, form):
        form.save()

        # Login a user right after registration
        user = authenticate(email=form.instance.email, password=form.cleaned_data["password1"])
        if user is not None:
            login(self.request, user)

        return super().form_valid(form)


class MessageListView(ListView):
    model = Message


class MessageCreateView(CreateView):
    model = Message
    fields = ('text', )
    success_url = reverse_lazy('zap:message_index')
