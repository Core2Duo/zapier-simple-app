import json
from django.views.generic import View
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

from .models import Message, User, Subscription


class ApiBaseView(View):
    """
    Basic View class for API classes.
    It checks user's ID and API key and returns 403 error if the credentials are wrong.
    On success returns API response.
    Also, it disables CSRF for child views for obvious reason.
    """
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        query_dict = request.GET

        if 'user_id' not in query_dict or 'api_key' not in query_dict:
            return HttpResponse('You must provide user_id and api_key', status=403)

        try:
            user_id = int(query_dict['user_id'])
            api_key = query_dict['api_key']
        except ValueError:
            return HttpResponse('user_id must be a number', status=403)

        try:
            User.objects.get(pk=user_id, api_key=api_key)
        except ObjectDoesNotExist:
            return HttpResponse('Wrong user_id or api_key', status=403)

        return super().dispatch(request, *args, **kwargs)


class ApiMessageListView(ApiBaseView):
    def get(self, request):
        messages = Message.objects.all()
        result = []
        for message in messages:
            result.append({
                'text': message.text
            })

        result = json.dumps(result)
        return HttpResponse(result, content_type='text/json')


class ApiSubscribeView(ApiBaseView):
    def post(self, request):
        body = json.loads(request.body.decode('utf-8'))
        url = body['target_url']
        event = body['event']

        # The user has been already verified
        user = User.objects.get(pk=request.GET['user_id'])

        subscription = Subscription(user=user, event=event, url=url)
        try:
            subscription.save()
        except IntegrityError:
            # Duplicate URL, due to docs we should return 409
            return HttpResponse(status=409)

        # If everything is okay return 201
        return HttpResponse(status=201)


class ApiUnsubscribeView(ApiBaseView):
    def post(self, request):
        body = json.loads(request.body.decode('utf-8'))
        url = body['target_url']

        try:
            sub = Subscription.objects.get(url=url)
            sub.delete()
        except ObjectDoesNotExist:
            # Already deleted or never created. Do nothing and just return 200
            pass

        return HttpResponse(status=200)
