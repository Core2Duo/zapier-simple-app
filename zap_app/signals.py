import json
from urllib.request import Request, urlopen
from urllib.error import HTTPError
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings

from .models import Message, Subscription


@receiver(post_save, sender=Message, dispatch_uid='message_post_save')
def message_post_save_handler(sender, **kwargs):
    message = kwargs['instance']

    # Zapier API expects an array of objects even there is only one
    payload = [
        {
            'text': message.text
        }
    ]
    payload = json.dumps(payload).encode('utf-8')

    # Get a list of subscribers and send them a ping
    subs = Subscription.objects.filter(event=settings.API_ZAPIER_MESSAGE_EVENT)
    for sub in subs:
        request = Request(sub.url, data=payload, headers={'content-type': 'text/json'})
        try:
            response = urlopen(request)
        except HTTPError as e:
            # Response code is not 200
            if e.code == 410:
                # If Zapier responds with a 410 status code we should immediately unsubscribe
                sub.delete()
