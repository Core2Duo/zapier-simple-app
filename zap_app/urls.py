from django.conf.urls import url
from django.contrib.auth.decorators import login_required as lr
from django.contrib.auth import views as auth_views

from .views import IndexView, UserRegisterView, UserProfileView, MessageCreateView, MessageListView
from .api_views import ApiMessageListView, ApiSubscribeView, ApiUnsubscribeView

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),

    # User and auth
    url(r'^login/$', auth_views.login, name='user_login', kwargs={'template_name': 'zap_app/user_login.html'}),
    url(r'^register/$', UserRegisterView.as_view(), name='user_register'),
    url(r'^profile/$', lr(UserProfileView.as_view()), name='user_profile'),
    url(r'^logout/$', auth_views.logout, name='user_logout', kwargs={'template_name': 'zap_app/user_logout.html'}),

    # Messages
    url(r'^messages/$', MessageListView.as_view(), name='message_index'),
    url(r'^messages/new$', MessageCreateView.as_view(), name='message_create'),

    # API
    # Subscription (REST hooks)
    url(r'^api/v1/subscribe/$', ApiSubscribeView.as_view(), name='api_subscribe'),
    url(r'^api/v1/unsubscribe/$', ApiUnsubscribeView.as_view(), name='api_unsubscribe'),
    # Poll calls
    url(r'^api/v1/messages/$', ApiMessageListView.as_view(), name='api_message_index'),
]
