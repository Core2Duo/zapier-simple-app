from django.apps import AppConfig


class ZapAppConfig(AppConfig):
    name = 'zap_app'

    def ready(self):
        import zap_app.signals
