import multiprocessing
from os.path import join, dirname, abspath

ROOT = dirname(dirname(abspath(__file__)))
LOGS_FOLDER = join(dirname(ROOT), 'logs')

bind = 'unix:' + join(dirname(ROOT), 'run', 'gunicorn.sock')

workers = multiprocessing.cpu_count() * 2 + 1
threads = multiprocessing.cpu_count() * 3

keepalive = 5

user = 'www-data'
group = 'www-data'

proc_name = 'gunicorn-zap'

accesslog = join(LOGS_FOLDER, 'gunicorn_access_log')
access_log_format = '%({x-real-ip}i)s %(l)s %(u)s %(t)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s" %(D)s'
errorlog = join(LOGS_FOLDER, 'gunicorn_error_log')
